let express = require('express');
let router = express.Router();
let bicicletaController = require('../controllers/bicicletaController');

//Home lista de bicis

router.get('/',bicicletaController.listaBicis);

//Nueva bici
router.get('/nueva',bicicletaController.nueva_bici_get);
router.post('/nueva',bicicletaController.nueva_bici_post);

//Editar bici
router.get('/editar/:id',bicicletaController.editar_bici_get);
router.post('/editar/:id',bicicletaController.editar_bici_post);

//vista Simple
router.get('/vista-simple/:id',bicicletaController.ver_bici_get);

//Eliminar bici
router.post('/eliminar',bicicletaController.eliminar_bici_post);

module.exports = router;
