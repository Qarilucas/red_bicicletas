const express = require('express');
const router = express.Router();
const authControllerApi = require('../../controllers/api/authControllerApi');
const passport = require('../../config/passport');

router.post('/authenticate', authControllerApi.autenticate);
router.post('/forgotPassword', authControllerApi.forgotPassword);
router.post('/facebook_token',passport.authenticate('facebook-token'),authControllerApi.authFacebookToken);

module.exports = router;