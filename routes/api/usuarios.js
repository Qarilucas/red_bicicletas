const express = require('express');
const router = express.Router();
const usuarioControllerApi = require('../../controllers/api/usuarioControllerApi');

router.get('/',usuarioControllerApi.usuarios_list);
router.post('/create',usuarioControllerApi.usuarios_create);
router.post('/reservar',usuarioControllerApi.usuarios_reservar);

module.exports = router;