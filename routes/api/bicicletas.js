let express = require('express');
let router = express.Router();
let bicicletaController = require('../../controllers/api/bicicletaControllerApi');

router.get('/',bicicletaController.bicicleta_list);
router.post('/crear-bici-api',bicicletaController.crear_bici_api);
router.post('/editar-bici-api/:id',bicicletaController.editar_bici_api);
router.delete('/eliminar-bici-api', bicicletaController.eliminar_bici_api);


module.exports = router;