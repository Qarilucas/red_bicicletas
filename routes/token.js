const express = require('express');
const router = express.Router();

const tokenController = require('../controllers/tokenController');

router.get('/confirmation/:token',tokenController.confirmationGet);

module.exports = router;