let Bicicleta = require('../models/bicicletaModel');
const Metodos = require('../metodos/biciCrud')

//Principal de bicicletas
exports.listaBicis = (req,res)=>{
   Bicicleta.listaBicis((err,bicis)=>{
      res.render('bicicletas/index',{bicis:bicis})
   });
   // res.render('bicicletas/index', {bicis:Bicicleta.listaBicis()});
}

//Nueva bicicleta

exports.nueva_bici_get = (req,res)=>{
   res.render('bicicletas/nueva');
}
exports.nueva_bici_post = (req,res)=>{
   let nueva = new Bicicleta({code:req.body.id,color:req.body.color,modelo:req.body.modelo,ubicacion:[req.body.lat,req.body.lng]}) 
   Bicicleta.addBici(nueva, err=>{
      if(err)console.log(err);
      res.redirect('/bicicletas');
   });
}

//Editar bicicleta

exports.editar_bici_get = (req,res)=>{
   Bicicleta.buscarBici(req.params.id,(err,bici)=>{
      if(err) console.log(err);
      res.render('bicicletas/editar',{bici})
   })
   let bici = Bicicleta.buscarBici(req.params.id,(err,bici)=>{if(err)console.log(err);})
}

exports.editar_bici_post=(req,res)=>{
   Bicicleta.buscarBici(req.params.id,(err,bici)=>{
      bici.code = req.body.id;
      bici.color = req.body.color;
      bici.modelo = req.body.modelo;
      bici.ubicacion = [req.body.lat,req.body.lng];
      res.redirect('/bicicletas')
   })   
}

//Vista individual
exports.ver_bici_get = (req,res)=>{
   const bici = Bicicleta.buscarBici(req.params.id,(err,bici)=>{
      if(err)console.log(err);
      res.render('bicicletas/vista-simple',{bici})
   })
}

//Eliminar Bici

exports.eliminar_bici_post = (req,res)=>{
   Metodos.eliminar_una(req.body.id);
   res.redirect('/bicicletas')
}