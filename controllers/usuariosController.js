const Usuario = require('../models/usuarioModel');


module.exports = {
   list: function(req,res,next){
      Usuario.find({},(err,usuarios)=>{
         res.render('usuarios/index',{usuarios:usuarios});
      });
   },
   update_get: function(req,res,next) {
      Usuario.findById(req.params.id,(err,usuario)=>{
         res.render('usuarios/update',{errors:{},usuario:usuario});
      });
   },
   update_post: function(req,res,next){
      const udpdateValues = {nombre:req.body.nombre};
      Usuario.findByIdAndUpdate(req.params.id,udpdateValues,(err,usuario)=>{
         if (err) {
            console.log(err);
            res.render('usuarios/update',{errors:err.errors, usuario: new Usuario({nombre:req.body.nombre,email:req.body.email})})
         }else{
            res.redirect('/usuarios');
            return;
         }
      })
   },
   create_get: function(req,res,next){
      res.render('usuarios/create',{errors:{}, usuario: new Usuario()})
   },
   create_post:function(req,res,next){
      if (req.body.password != req.body.confirm_password) {
         res.render('usuarios/create',{errors:{confirm_password:{message:'No coinciden las claves'}}, usuario:new Usuario({nombre:req.body.nombre,email:req.body.email})});
         
         return;
      }

      Usuario.create({nombre:req.body.nombre,email:req.body.email,password:req.body.password},(err,nuevoUsuario)=>{
         if (err) {
            res.render('usuarios/create',{errors:err.errors, usuario: new Usuario({nombre:req.body.nombre,email:req.body.email})})
            console.log(err);
         } else {
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios');
         }
      });
   },
   delete_post:function(req,res,next) {
      Usuario.findByIdAndDelete(req.body.id,(err)=>{
         if (err) {
            next(err)
         }else{
            res.redirect('/usuarios');
         }
      })
   }
}