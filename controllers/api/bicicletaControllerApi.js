let Bicicleta = require('../../models/bicicletaModel');
const Metodos = require('../../metodos/biciCrud')

//METODOS PERSISENCIA
exports.bicicleta_list = (req,res)=>{
   Bicicleta.listaBicis((err,bicis)=>{
      res.status(200).json({
         bicis:bicis
      })
   })
}



exports.crear_bici_api = (req,res)=>{
   const bici = new Bicicleta({code:req.body.id,color:req.body.color,modelo:req.body.modelo,ubicacion:[req.body.lat,req.body.lng]});
   Bicicleta.addBici(bici);
   res.status(200).json({
      bici:bici
   })
}

exports.editar_bici_api = (req,res)=>{
   const bici = Metodos.buscar(req.params.id);
   bici.code = req.body.id;
   bici.color = req.body.color;
   bici.modelo = req.body.modelo;
   bici.ubicacion = [req.body.lat,req.body.lng];
   res.status(200).send({
      bici:bici
   });
}

exports.eliminar_bici_api = (req,res)=>{
   Metodos.eliminar(req.body.code);
   res.status(204).send()
}