const Bicicleta = require('../../models/bicicletaModel');
const request=require('request');
const server = require('../../bin/www');
const mongoose = require('mongoose');


const base_url = 'http://localhost:3000/api/bicicletas';

//API EN BD

describe('Bicicleta Api',()=>{
   //conexion
   beforeAll((done) => { mongoose.connection.close(done) });
   beforeEach(done=>{
      const mongoDB = 'mongodb://localhost/testdb';
      mongoose.connect(mongoDB,{useNewUrlParser:true,useUnifiedTopology:true,useCreateIndex:true});
      const db = mongoose.connection;
      db.on('error',console.error.bind(console,'connection error'));
      db.on('open',()=>{
         console.log('Conectado desde api de bicis');
         done();
      });
   });
   
   afterEach(done=>{
      Bicicleta.deleteMany({},(err,success)=>{
         if (err) console.log(err);
         mongoose.disconnect();
         done();
      });
   });
   //Lista Bicis
   describe('GET Bicicletas /',()=>{
      it('Staus 200 / empieza vacía',done=>{
            request.get(base_url,(error,response,body)=>{
            const result = JSON.parse(body);
            expect(response.statusCode).toBe(200);
            expect(result.bicis.length).toBe(0);
            done();
         });
      });
   });
   //Add bici
   describe('Agregar bici a través de la api',()=>{
      it('Status 200',done=>{
         const headers = {'content-type':'application/json'};
         const bici = '{"id":1,"color":"Magenta","modelo":"BMX","lat":-0.192831,"lng":-78.493631}';
         request.post({
            headers:headers,
            url:base_url+'/crear-bici-api',
            body: bici,
         },(err,response,body)=>{
            expect(response.statusCode).toBe(200);
            const result = JSON.parse(body);
            expect(result.bici.color).toBe('Magenta');
            done();
         });
      });
   });
   //Editar bici
   describe('Edita bici por id',()=>{
      it('Status 200',done=>{
         const headers = {'content-type':'application/json'};
         const biciEdit = '{"id":1,"color":"Magenta","modelo":"BMX","lat":-0.192831,"lng":-78.493631}';
         request.post({
            headers:headers,
            body:biciEdit,
            url: 'http://localhost:3000/api/bicicletas/editar-bici-api/1'
         },(err,response,body)=>{
            expect(response.statusCode).toBe(200);
            const result = JSON.parse(body);
            expect(result.bici.color).toBe('Magenta')
            done();
         });
      })
   });
   //Delete Bici
   describe('Elimina una bici /eliminar-bici-api',()=>{
      it('Elimina una bici por id',done=>{
         const bici = new Bicicleta({code:1,color:'Magenta',modelo:'BMX',ubicacion:[-0.192831, -78.493631]});
         Bicicleta.addBici(bici);

         request.delete({
            headers:{'content-type':'application/json'},
            body: '{"id":1}',
            url: base_url + '/eliminar-bici-api'
         },(err,response,body)=>{
            expect(response.statusCode).toBe(204)
            console.log(body);
            // const result = JSON.parse(body);
            // expect(result.bicis.length).toBe(0);
            done();
         })
      })
   })
});
////SOLO API EN MEMORIA
// describe('Bicicleta API',()=>{
//    //Listar Bicis
//    describe('GET BICICLETAS /', ()=>{
//       it('Status 200',()=>{
//          expect(Bicicleta.listaBicis.length).toBe(0);
//          const Bici = new Bicicleta(2,'Magenta','BMX',[-0.192831, -78.493631]);
//          Bicicleta.addBici(Bici);
         
//          request.get('http://localhost:3000/api/bicicletas', (error,response,body)=>{
//             expect(response.statusCode).toBe(200);
//          });
//       });
//    });
//    //Crear Bici
//    describe('POST BICICLETAS /crear-bici-api',()=>{
//       it('Status 200', done=>{
//          const headers = {"content-type":"application/json"};
//          const bici = '{"id":3, "color":"purpura", "modelo":"Cros", "lat":3, "lng":4 }';

//          request.post({
//             headers: headers,
//             url:     'http://localhost:3000/api/bicicletas/crear-bici-api',
//             body:    bici
//          },
//          (error,response,body)=>{
//             expect(response.statusCode).toBe(200);
//             expect(Bicicleta.buscarBici(3).modelo).toBe('Cros');
//             done();
//          });
//       });
//    });
//    //Editar Bici
//    describe('POST BICICLETAS /editar-bici-api/:id',()=>{
//       it('Status 200',done=>{
//          const Bici = new Bicicleta(1,'Magenta','BMX',[-0.192831, -78.493631]);
//          Bicicleta.addBici(Bici);
         
//          const headers = {"content-type":"application/json"};
//          const biciEdit = '{"id":1, "color":"morado", "modelo":"ruta", "lat":10, "lng": 20 }';
//          request.post({
//             headers:    headers,
//             url:        'http://localhost:3000/api/bicicletas/editar-bici-api/1',
//             body:       biciEdit
//          },
//          (error,response,body)=>{
//             expect(response.statusCode).toBe(200);
//             expect(Bicicleta.buscarBici(1).color).toBe('morado');
//             expect(Bicicleta.buscarBici(1).modelo).toBe('ruta');
//             done();
//          });
//       })
//    });

//    //Eliminar bici
//    describe('ELIMINAR BICI /eliminar-bici-api',()=>{
//       it('Status 204', (done)=>{
//          const headers = {'content-type':'application/json'};
//          const body = '{"id":1}';
//          Bicicleta.listaBicis = [];
//          const Bici = new Bicicleta(1,'Magenta','BMX',[-0.192831, -78.493631]);
//          Bicicleta.addBici(Bici);

//          request.delete({
//             headers:headers,
//             url: 'http://localhost:3000/api/bicicletas/eliminar-bici-api',
//             body:body
//          },
//          (error,response,body)=>{
//             expect(response.statusCode).toBe(204);
//             expect(Bicicleta.listaBicis.length).toBe(0);
//             done()
//          });
//       });
//    });
// });
