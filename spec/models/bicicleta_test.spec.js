const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicletaModel');

describe('Testing bicicletas',()=>{
   beforeEach(done=>{
      const mongoDB = 'mongodb://localhost/testdb';
      mongoose.connect(mongoDB,{useNewUrlParser:true,useUnifiedTopology:true,useCreateIndex:true})

      const db = mongoose.connection;
      db.on('error',console.error.bind(console,'connection error'));
      db.once('open',()=>{
         console.log('We are connected to test database');
         done();
      });
   });

   afterEach(done=>{
      Bicicleta.deleteMany({},(err,success)=>{
         if (err) console.log(err);
         mongoose.disconnect(err);
         done();
      });
   });

   describe('Bicicleta.createIntance',()=>{
      it('Crea una instancia de bicicleta', ()=>{
         const bici = Bicicleta.createInstance(1,'Magenta','BMX',[-0.192831, -78.493631]);
         
         expect(bici.code).toBe(1);
         expect(bici.color).toBe('Magenta');
         expect(bici.modelo).toBe('BMX');
         expect(bici.ubicacion[0]).toEqual(-0.192831);
         expect(bici.ubicacion[1]).toEqual(-78.493631);
      });
   });

   describe('Bicicleta.listaBicis',()=>{
      it('Comienza Vacía',(done)=>{
         Bicicleta.listaBicis((err,bicis)=>{
            expect(bicis.length).toBe(0);
            done();
         });
      });
   });

   describe('Bicicletas.add',()=>{
      it('Agrega solo una bici', done=>{
         const bici = new Bicicleta({code:1,color:'Magentttttta',modelo:'BMX',ubicacion:[-0.192831, -78.493631]});
         Bicicleta.addBici(bici, (err,newBici)=>{
            if (err) console.log(err);
            Bicicleta.listaBicis((err,bicis)=>{
               expect(bicis.length).toEqual(1);
               expect(bicis[0].code).toEqual(bici.code);
               done();
            });
         });
      });
   });

   describe('Bicicletas.buscarBici',()=>{
      it('Busca bicicleta por codigo "id" para el ejemplo es code 1',done=>{
         Bicicleta.listaBicis((err,bicis)=>{
            if (err) console.log(err);
            expect(bicis.length).toBe(0);

            const bici = new Bicicleta({code:1,color:'Magenta',modelo:'BMX',ubicacion:[-0.192831, -78.493631]});

            Bicicleta.addBici(bici,(err,newBici)=>{
               if (err) console.log(err);
               
               const bici2 = new Bicicleta({code:2,color:'hierro',modelo:'ruta',ubicacion:[3,4]});
               Bicicleta.addBici(bici2,(err,newBici2)=>{
                  if(err) console.log(err);
                     Bicicleta.buscarBici(2,(err,targetBici)=>{
                     if (err) console.log(err);
   
                     expect(targetBici.code).toBe(bici2.code);
                     expect(targetBici.color).toBe(bici2.color);
                     expect(targetBici.modelo).toBe(bici2.modelo);
                     done();
                  })
               })
            })
         })
      });
   });
   
   describe('Bicicleta.removeBici',()=>{
      it('Elimina una bicicleta por code "id" en este caso id 1',done=>{
         const bici = new Bicicleta({code:1,color:'Magenta',modelo:'BMX',ubicacion:[-0.192831, -78.493631]});
         Bicicleta.addBici(bici,(err,newBici)=>{
            if (err) console.log(err);
            Bicicleta.removeBici(1,(err,targetBici)=>{
               if(err)console.log(err);
               done();
            })
         })
      })
   });
});

//METODOS DE TESTEO CON LA CARGA EN MEMORIA
// beforeEach(()=>{Bicicleta.listaBicis=[]});

// //comprobar lista de bicis
// describe('Bicicletas.listaBicis', ()=>{
//    it('Lista de bicis inicia bacía',()=>{
//       expect(Bicicleta.listaBicis.length).toBe(0);
//    })
// });

// //Añadir bici

// describe('Bicicleta.addBici',()=>{
//    it('Añade una Bici', ()=>{
//       expect(Bicicleta.listaBicis.length).toBe(0);
//       const Bici = new Bicicleta(2,'Magenta','BMX',[-0.192831, -78.493631]);
//       Bicicleta.addBici(Bici);
//       expect(Bicicleta.listaBicis[0]).toBe(Bici);
//    })
// })

// //Eliminar bici

// describe('Bicicleta.removeBici',()=>{
//    it('Elimina una bicicleta por el id indicado como parametro',()=>{
//       expect(Bicicleta.listaBicis.length).toBe(0);
//       const Bici = new Bicicleta(1,'Magenta','BMX',[-0.192831, -78.493631]);
//       Bicicleta.addBici(Bici);
//       Bicicleta.removeBici(1)
//       expect(Bicicleta.listaBicis.length).toBe(0);
//    })
// });

// //Buscar Bici

// describe('Bicicleta.buscarBici',()=>{
//    it('Busca una bicicleta por el id indicado como parametro',()=>{
//       expect(Bicicleta.listaBicis.length).toBe(0);
//       const Bici = new Bicicleta(1,'Magenta','BMX',[-0.192831, -78.493631]);
//       Bicicleta.addBici(Bici);
//       const targetBici = Bicicleta.buscarBici(1)
//       expect(targetBici.id).toBe(Bici.id);
//       expect(targetBici.color).toBe(Bici.color);
//       expect(targetBici.modelo).toBe(Bici.modelo);
//       expect(targetBici.ubicacion).toBe(Bici.ubicacion);
//    })
// })