const Reserva = require('../../models/reservaModel');
const Usuario = require('../../models/usuarioModel');
const Bicicleta = require('../../models/bicicletaModel');
const mongoose = require('mongoose');


describe('test de usuarios',()=>{
   
   beforeEach(function(done){
      mongoose.disconnect();
      const mongoDB = 'mongodb://localhost/testdb';
      mongoose.connect(mongoDB,{useNewUrlParser:true,useUnifiedTopology:true,useCreateIndex:true,});
      const db = mongoose.connection;
      db.on('error',console.error.bind(console,'Error de conexión'));
      db.once('open',()=>{
         console.log('Conectado');
         done();
      })
   });
   afterEach(function(done){
      Reserva.deleteMany({},(err,success)=>{
         if(err)console.log(err);
         done()
         Usuario.deleteMany({},(err,success)=>{
            if(err)console.log(err);
            done()
            Bicicleta.deleteMany({},(err,success)=>{
               if(err)console.log(err);
               done();
            })
         })
      })
   });
   
   describe('Cuando un usuario reserva una bici',()=>{
      it('desde existir la reserva',done=>{
         const usuario = new Usuario({nombre:'Carlos'});
         usuario.save();
         const bicicleta = new Bicicleta({code:1,color:'Magenta',modelo:'BMX',ubicacion:[-0.192831, -78.493631]});
         bicicleta.save();

         let hoy = new Date();
         let mañana = new Date();
         mañana.setDate(hoy.getDate()+1);

         usuario.reservar(bicicleta.id,hoy,mañana,function(err,reserva){
            if(err)console.log(err);
            Reserva.find({}).populate('bicicleta').populate('usuario').exec((err,reservas)=>{
               expect(reservas.length).toBe(1);
               expect(reservas[0].diasDeReserva()).toBe(2);
               expect(reservas[0].bicicleta.code).toBe(1);
               expect(reservas[0].usuario.nombre).toBe('Carlos');
               console.log(reservas[0]);
               done()
            });
         })
      })
   })
   
})