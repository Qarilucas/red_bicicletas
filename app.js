require('./newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const Usuario = require('./models/usuarioModel');
const Token = require('./models/tokenModel');
const jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);

var indexRouter = require('./routes/index');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');


let store;
if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error',(error)=>{
    assert.ifError(error);
    assert.ok(false);
  })
}

var app = express();
app.set('secretKey', '3lPwd4s1!gn#4D0');
app.use(session(
  {
    cookie:{maxAge:240 * 60 * 60 * 100},
    store: store,
    saveUninitialized: true,
    resave:true,
    secret:'rep0b3w_d_bA4E3I1O0icis@#|p0b3w_A4E3I1O0'
  }
));

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//CONEXION A BASE DE DATOS CON MONGOOSE
var mongoose = require('mongoose');
const { decode } = require('punycode');
const { assert } = require('console');

// var mongoDB = 'mongodb://localhost/red_bicicletas';
var mongoDB = process.env.MONGO_URI;

// var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB,{useNewUrlParser:true,useUnifiedTopology:true,useCreateIndex:true});
mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error',console.error.bind(console,'MongoDB connection error: '));

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuarioApi,bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/api/auth', authApiRouter);

app.use('/privacy-policy',(req,res)=>{
  res.sendFile('/privacy_policy.html',{root:'public'});
});

app.use('/googlea1a27716f5296363.html',(req,res)=>{
  res.sendFile('/googlea1a27716f5296363.html',{root:'public'});
});

//+++++++++++++OAUTH GOOGLE+++++++

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read',
    'profile',
    'email'
    ] 
  })
);

app.get('/auth/google/callback', 
  passport.authenticate('google', { 
    successRedirect: '/',
    failureRedirect: '/error' 
  })
);

// app.get('/auth/google',
//   passport.authenticate('google', { scope: ['profile'] }));

// app.get('/auth/google/callback', 
//   passport.authenticate('google', { failureRedirect: '/login' }),
//   function(req, res) {
//     // Successful authentication, redirect home.
//     res.redirect('/');
// });

//+++++++++++++ FIN OAUTH GOOGLE+++++++

//INICIO CONFIGURACION DE LOGIN
app.get('/login', (req,res)=>{
  res.render('session/login', )
});

app.post('/login',(req,res,next)=>{
  passport.authenticate('local',(err,usuario,info)=>{
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});
    req.logIn(usuario,(err)=>{
      if(err) return next(err);
      return res.redirect('/bicicletas')
    });

  })(req,res,next)
})

app.get('/logout',(req,res)=>{
  req.logOut()
  res.redirect('/');
})
 
app.get('/forgotPassword',(req,res)=>{
  res.render('session/forgotPassword');
});

app.post('/forgotPassword',(req,res)=>{
  Usuario.findOne({email:req.body.email},(err,usuario)=>{
    if(err)console.log(err);
    if(!usuario){
      return res.render('session/forgotPassword',{info:{message:'Este email no está asociado a ningún usuario existente.'}});
    }
    usuario.resetPassword(err=>{
      if(err) console.log(err);
    })
    res.render('session/forgotPasswordMessage')
  });
});


app.get('/resetPassword/:token',(req,res,next)=>{
  Token.findOne({token:req.params.token},(err,token)=>{
    if(!token){
      return res.status(400).send({
        type:'not-verified',
        msg:'No existe un usuario asociado a este token. Verifique que no haya expirado.'
      })
    }
    console.log('token userid: '+token._userId);
    Usuario.findById(token._userId,function(err,usuario){
      if(!usuario) return res.status(400).send({msg:'No existe un usuario asociado al token.'});
      res.render('session/resetPassword',{errors:{},usuario:usuario});
    });
  });
});

app.post('/resetPassword',(req,res)=>{
  if (req.body.password !== req.body.confirm_password) {
    res.render('session/resetPassword', {
      errors:{confirm_password:{message:'Las contraseñas no coinciden'}},
      usuario: new Usuario({email:req.body.email})
  });
  return;
  };
  Usuario.findOne({email:req.body.email},(error,usuario)=>{
    usuario.password = req.body.password;
    usuario.save(err=>{
      if (err) {
        res.render('session/resetPassword',{errors:err.errors, usuario: new Usuario({email:req.body.email})});
      }else{
        res.redirect('/login')
      }
    });
  });
});
//FIN DE CONFIGURACION DE LOGIN

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn (req,res,next){
  if (req.user) {
    next();
  }else{
    console.log('Usuario sin logearse');
    res.redirect('/login');
  }
}

function validarUsuarioApi(req,res,next){
  jwt.verify(req.headers['x-access-token'],req.app.get('secretKey'),(err,decoded)=>{
    if (err) {
      res.json({status:'error',message:err.message,data: null})
    } else {
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  })
}

module.exports = app;
