const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuarioModel');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new localStrategy(
   function (email,password,done) {
      Usuario.findOne({email:email},(err,usuario)=>{
         if(err) return done(err);
         if(!usuario) return done(null,false,{message:'El email no existe'});

         if(!usuario.validPassword(password)) return done(null,false,{message:'Password incorrecto'});

         return done(null,usuario)
      })
   }
));

passport.use(new GoogleStrategy({
   clientID: process.env.GOOGLE_CLIENT_ID,
   clientSecret: process.env.GOOGLE_CLIENT_SECRET,
   callbackURL: process.env.HOST + '/auth/google/callback'
},
   (accessToken,refreshToken,profile,cb)=>{
      console.log('Este es el perfil: ' + profile);
      Usuario.findOneOrCreateByGoogle(profile, (err,user)=>{
         return cb(err,user);
      })
   }
));

passport.use(new FacebookTokenStrategy({
   clientID: process.env.FACEBOOK_ID,
   clientSecret: process.env.FACEBOOK_SECRET
   },(accessToken,refreshToken,profile,done)=>{
      try{
         Usuario.findOneOrCreateByFacebook(profile,(err,user)=>{
            if(err)console.log('El error de conexión facebook: ' + err);
            return done(err,user)
         })
      }catch(err2){
         console.log(err2);
         return done(err2,null);
      }
   })
);

passport.serializeUser((user,cb)=>{
   cb(null,user.id);
});

passport.deserializeUser((id,cb)=>{
   Usuario.findById(id,(err,usuario)=>{
      cb(err, usuario);
   })
})

module.exports = passport;