const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
   _userId: {
      type:mongoose.Schema.Types.ObjectId, 
      ref: 'Usuario',
      required:true
   },
   token: {
      type:String,
      required:true
   },
   createdAt: {
      type:Date,
      required:true,
      default:Date.now,
      expires: 43200
   },
})

module.exports = mongoose.model('Token',tokenSchema);