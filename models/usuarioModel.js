const mongoose = require('mongoose');
const Reserva = require('./reservaModel');
const bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const Token = require('./tokenModel');
const crypto = require('crypto');
const mailer = require('../mailer/mailer');
const { sendMail, getMaxListeners } = require('../mailer/mailer');


const saltRounds = 10;

const validateEmail = (email)=>{
   const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return email.match(regExp);
}

const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
   nombre:{
      type:String,
      trim:true,
      required:[true,'El nomre es obligatorio.']
   },
   email:{
      type:String,
      trim:true,
      required:[true,'El email es obligatorio.'],
      lowercase:true,
      validate:[validateEmail,'Ingrese un correo válido.'],
      match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/],
      unique:true,
   },
   password:{
      type:String,
      trim:true,
      required:[true,'El password es obligatorio']
   },
   passwordResetToken: String,
   passwordResetTokenExpires:Date,
   verificado:{
      type:Boolean,
      default:false
   },
   googleId:String,
   facebookId:String,
});

usuarioSchema.plugin(uniqueValidator,{message:'El {PATH} ya existe con otro usuario.'})

usuarioSchema.pre('save',function(next){
   if(this.isModified('password')){
      this.password=  bcrypt.hashSync(this.password,saltRounds);
   };
   next();
});

usuarioSchema.methods.validPassword = function(password){
   console.log(this.password);
   console.log(password);
   console.log(bcrypt.compareSync(password,this.password));
   
   return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar = function(biciId,desde,hasta,callback){
   const reserva = new Reserva({desde:desde,hasta,hasta,bicicleta:biciId,usuario:this._id});
   console.log(reserva);
   reserva.save(callback);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
   const token = new Token({_userId:this.id,token:crypto.randomBytes(16).toString('hex')});
   const email_desnination = this.email;
   token.save((err)=>{
      if (err) {
         return console.log(err.message);
      }

      const mailOptions = {
         from:'no-reply@redbicicletas.com',
         to:email_desnination,
         subject:'Verificacion de cuenta red de bicis',
         html:'Hola,\n\n' + 'Por favor para verificar la cuenta haz clic en: \n' + '<a href="http://localhost:3000' + '\/token/confirmation\/' + token.token + '" target="blank">Link</a>\n'
      };

      mailer.sendMail(mailOptions,function(err){
         if (err) {
            return console.log(err.message);
         }
         console.log('Un email de verificación ha sido enviado a' + email_desnination)
      });

   })
};

//RESETEAR CLAVE
usuarioSchema.methods.resetPassword = function(cb){
   const token = new Token({_userId:this.id,token:crypto.randomBytes(16).toString('hex')});
   const email_destination = this.email;
   
   token.save(err=>{
      if(err) console.log('ERROR SAVE: '+err.message);

      const mailOptions={
         from: 'no-reply@redbicis.com',
         to:email_destination,
         subject: 'Reseteo de password de cuenta',
         html:'Hola,\n\n' + 'Por favor para resetear el password haz clic en: \n' + '<a href="http://localhost:3000' + '\/resetPassword\/' + token.token + '" target="_blank">Resetear</a>\n'
      }
      mailer.sendMail(mailOptions,(err)=>{
         if (err) {
            return console.log('ERROR: '+err.message);
         }
         console.log('Un email de verificación ha sido enviado a' + email_desnination)
      });

      
   })
}

//+++++++BUSCAR O CREAR USUARIO CON OAUTH GOOGLE

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
   const self = this;
   console.log(condition);

   self.findOne({
      $or:[
         {'googleId':condition.id},{email:condition.emails[0].value}
      ]
   },(err,result)=>{
      if (result) {
         callback(err,result)
      } else {
         console.log('-------------------CONDITION-------------------');
         console.log(condition);
         let values = {};
         values.googleId = condition.id;
         values.email = condition.emails[0].value;
         values.nombre = condition.displayName || 'SIN NOMBRE';
         values.verificado = true;
         values.password = condition._json.etag;
         console.log('-------------------VALUES-------------------');
         console.log('Estos son los Values '+values);
         self.create(values,(err,result)=>{
            if(err) console.log(err);
            return callback(err,result);
         })
      }
   });
}
//+++++++BUSCAR O CREAR USUARIO CON OAUTH FACEBOOK
usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
   const self = this;
   console.log(condition);

   self.findOne({
      $or:[
         {'facebookId':condition.id},{'email':condition.emails[0].value}
      ]
   },(err,result)=>{
      if (result) {
         callback(err,result)
      } else {
         console.log('-------------------CONDITION-------------------');
         console.log(condition);
         let values = {};
         values.facebookId = condition.id;
         values.email = condition.emails[0].value;
         values.nombre = condition.displayName || 'SIN NOMBRE';
         values.verificado = true;
         values.password = crypto.randomBytes(16).toString('hex');
         console.log('-------------------VALUES-------------------');
         console.log('Estos son los Values '+values);
         self.create(values,(err,result)=>{
            if(err) console.log(err);
            return callback(err,result);
         })
      }
   });
}

const Usuario = mongoose.model('Usuario',usuarioSchema);
module.exports = Usuario;