//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//METODOS DE BICICLETA CON MONGOOSE

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
   code:Number,
   color:String,
   modelo:String,
   ubicacion: {
      type:[Number],
      index:{
         type:'2dsphere',
         sparse:true
      }
   }
});

bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
   return new this({
      code:code,
      color:color,
      modelo:modelo,
      ubicacion:ubicacion
   });
};

bicicletaSchema.methods.toString = function(){
   return `id: ${this.code} color: ${this.color}`
}

bicicletaSchema.statics.listaBicis = function(callback){
   return this.find({},callback);
}

bicicletaSchema.statics.addBici = function(bicicleta,callback){
   return this.create(bicicleta,callback);
}

bicicletaSchema.statics.buscarBici = function(id,callback) {
   return this.findOne({_id:id},callback);
}

bicicletaSchema.statics.removeBici = function(id,callback) {
   return this.deleteOne({_id:id},callback);
}

const Bicicleta = mongoose.model('Bicicleta',bicicletaSchema);

module.exports = Bicicleta;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//METODOS QUE GUARDAN EN MEMORIA
// class Bicicleta{
//    constructor(id,color,modelo,ubicacion){
//       this.id = id;
//       this.color = color;
//       this.modelo = modelo;
//       this.ubicacion = ubicacion;
//    }
// }

// Bicicleta.prototype.toString = ()=>{
//    return `id: ${this.id} color: ${this.color}`;
// }

// Bicicleta.listaBicis = [];

// Bicicleta.addBici = bici=>{
//    Bicicleta.listaBicis.push(bici);
// }

// Bicicleta.removeBici = biciId =>{
//    for (let i = 0; i < Bicicleta.listaBicis.length; i++) {
//       if(Bicicleta.listaBicis[i].id==biciId){
//          Bicicleta.listaBicis.splice(i,1)
//       }      
//    }
// }

// Bicicleta.buscarBici = biciId =>{
//    let bici = Bicicleta.listaBicis.find(x=>x.id==biciId);
//    if (bici) {
//       return bici;
//    } else {
//       throw new Error(`La bici con el id: ${biciId} no existe`)
//    }
// }

// let montanera = new Bicicleta(1,'Cyan','Montañera',[-0.193501, -78.494378]);
// let bmx = new Bicicleta(2,'Magenta','BMX',[-0.192831, -78.493631]);
// let rutera = new Bicicleta(3,'Amarillo','Rutera',[-0.194456, -78.494276]);

// Bicicleta.addBici(montanera);
// Bicicleta.addBici(bmx);
// Bicicleta.addBici(rutera);

// module.exports = Bicicleta;
