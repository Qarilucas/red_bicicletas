var mymap = L.map('map').setView([-0.194253, -78.494200], 16);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoicWFyaWx1Y2FzIiwiYSI6ImNrZm9nbTg0dzAybW4ycXFxeGtocWo4Nm0ifQ.X_55ljZ1iCcZ_zvbiNVrfw'
}).addTo(mymap);

// var marker1 = L.marker([-0.193501, -78.494378]).addTo(mymap);
// var marker2 = L.marker([-0.192831, -78.493631]).addTo(mymap);
// var marker3 = L.marker([-0.194456, -78.494276]).addTo(mymap);
$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success:result=>{
        
        result.bicis.forEach(bici => {
            L.marker(bici.ubicacion,{title:bici.color}).addTo(mymap)
        });
    }
})

var circle = L.circle([-0.194253, -78.494200], {
   color: 'red',
   fillColor: '#f03',
   fillOpacity: 0.5,
   radius: 250
}).addTo(mymap);