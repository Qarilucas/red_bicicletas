const nodemailer = require('nodemailer');
const sgTranspot = require('nodemailer-sendgrid-transport');

let mailConfig;

if (process.env.NODE_ENV == 'production') {
    const options = {
        auth:{
            api_key:process.env.SENDGRID_API_SECRET,
        }
    }
    mailConfig = sgTranspot(options);
} else {
    if (process.env.NODE_ENV == 'staging') {
        console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
        const options = {
            auth: process.env.SENDGRID_API_SECRET
        }
        mailConfig = sgTranspot(options);
    } else {
        //Se sigue usando ethereal mail
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_password
            }
        }
    
    }
}
// configuracióon para envío local con ethereal mail
// const mailConfig = {
//    host: 'smtp.ethereal.email',
//    port: 587,
//    auth: {
//        user: 'rosanna14@ethereal.email',
//        pass: 'gNWYatgVZfvvVWTbXx'
//    }
// }

module.exports = nodemailer.createTransport(mailConfig );