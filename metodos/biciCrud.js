const { response } = require('express');
const Bicicleta = require('../models/bicicletaModel')

exports.crear = async(code,color,modelo,lat,lng)=>{
   const bici = new Bicicleta({code:code,color:color,modelo:modelo,ubicacion:[lat,lng]});
   await bici.save();
}


exports.buscar = async(code)=>{
   const bici = await Bicicleta.findOne({code:code});
   return bici;
}

exports.eliminar = async(code)=>{
   const bici = await Bicicleta.deleteMany({code:code});
}
exports.eliminar_una = async(id)=>{
   const bici = await Bicicleta.deleteOne({_id:id},err=>{
      if(err)console.log(err);
   });
   console.log('eliminada')
   
}